*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${browser}  chrome
${url}    https://zaiso-qa02.isometrix.net/isometrix.solutions.v4/default.aspx


*** Keywords ***
LoginTest
    [Arguments]    ${username}    ${password}
    open browser    ${url}  ${browser}
    maximize browser window
    #Enter login details
    #[Arguments]    ${username}
    input text    id:txtUsername    ${username}
    #[Arguments]    ${password}
    input text    id:txtPassword   ${password}
    click element    xpath://div[@id='btnLoginSubmit']
    capture page screenshot
    title should be    IsoMetrix
    capture page screenshot    IsometrixSuccessfullLogin.png

NaviagteToRiskRegister
    #Switch frame
    sleep    5 seconds
    Select Frame    xpath://iframe[@id='ifrMain']
    #Click on EHS
    sleep    2 seconds
    element should be visible    xpath://label[text()='Environment, Health & Safety']
    click element   xpath://label[text()='Environment, Health & Safety']
    capture page screenshot
    #Click on Risk Management
    sleep    2 seconds
    #set selenium speed    2 seconds
    element should be visible    xpath://label[text()='Risk Management']
    click element   xpath://label[text()='Risk Management']
    capture page screenshot
    #Click on Risk Register
    sleep    2 seconds
    #set selenium speed    2 seconds
    element should be visible    xpath://label[text()='Risk Register']
    click element   xpath://label[text()='Risk Register']
    capture page screenshot

ClickOnAddButton
    #Add button
    set selenium implicit wait    10 seconds
    click element    id:btnActAddNew
    #Validate Risk register scope & Supporting Document tabs
    element should be visible    xpath://li[@id='tab_0029D15E-8A6A-403E-8C5F-67B994DA330B']
    element should be visible    xpath://li[@id='tab_12BC3724-FC3C-458F-9348-D095B45903A7']
    #Validate Reason for request = New Risk
    element text should be    //div[@id='control_460A1A95-6C2F-4764-9631-1DDB8912196B']//li[text()='New risk']    New risk
    #Validate Date of Change, Date requested and Time requested
    element should be disabled    //div[@id='control_AD33A26E-B9EF-419D-9DF5-BE824BD225B4']//input
    element should be disabled    //div[@id='control_FBE7C43B-D18C-4F38-AF61-00D67CC4D70E']//input
    element should be disabled    //div[@id='control_3565E813-6C4B-48CE-85C0-B820CAABD119']//input
    #Validate Processflow = 1. Logging Risk Register
    click element   id:btnProcessFlow_form_3AB45051-222E-416C-AAD3-86B2EDA98BED
    element text should be    //div[@class='step active']//div[text()='1. Logging Risk Register']    1. Logging Risk Register

AnnualReview
     #click element   xpath://div[@id='control_460A1A95-6C2F-4764-9631-1DDB8912196B']//li
     click element    xpath://a[text()='Annual review']

AsresultofIncident
    click element    xpath://a[text()='As result of an incident']
    element should be visible    xpath://div[@id='control_75168AEA-F534-4B1D-A38A-FBA786BCF308']//li
    click element    xpath://div[@id='control_75168AEA-F534-4B1D-A38A-FBA786BCF308']//li
    click element    xpath://a[text()='#13: Fire accident']

Other
    [Arguments]    ${Other}
    click element    xpath://a[text()='Other (Please specify)']
    element should be visible    xpath:(//div[@id='control_E05537A2-F2F0-40F0-A0A0-1FBDD030BA37']//input)[1]
    input text    xpath:(//div[@id='control_E05537A2-F2F0-40F0-A0A0-1FBDD030BA37']//input)[1]    ${Other}

BusinessUnit
    #Business Unit dropdown
    click element    id:control_A1B23A4E-893B-46AB-9C47-AF967BE14AB7
    click element    xpath://a[text()='Global Company']/../i
    click element    xpath://a[text()='South Africa']/../i
    click element    xpath://a[text()='Victory Site']

ProjectCheckbox
    #Project checkbox and related fields
    click element    xpath://div[@id='control_2E6AB32F-5F8A-49BC-88E1-A8CD606E9B11']//div[@class='c-chk']
    element should be visible    id:control_8211E2EF-0E29-4A28-8689-206A31258F13
    element should be visible    id:control_3553F3D3-D76D-4205-A506-7450A6163C03
    click element    id:control_8211E2EF-0E29-4A28-8689-206A31258F13
    #wait until element is visible    xpath://a[text()='Expansion of clinic']
    click element    xpath://a[text()='Expansion of clinic']
    click element    id:control_3553F3D3-D76D-4205-A506-7450A6163C03
    #wait until element is visible    xpath://a[text()='01Vanilla Ink SE_V4.15.1']
    click element    xpath://a[text()='01Vanilla Ink SE_V4.15.1']
    capture page screenshot

CaptureRemainingDetails
    #Risk register title & Risk scope text fields
    [Arguments]    ${RiskTitle}    ${RiskScope}
    input text    xpath:(//div[@id='control_A280BA0F-593B-4A15-A93D-4B2522C64ED7']//input)[1]    ${RiskTitle}
    #[Arguments]    ${RiskScope}
    input text    xpath://div[@id='control_ED3FCD7C-F73F-4CE8-9030-08CE80DDA7CD']//textarea    ${RiskScope}

    #Impact type checklist dropdown
    click element    id:control_D0E8F3FA-BD27-4584-B51E-6B49F16D0480
    sleep    2 seconds
    #wait until element is visible    xpath:(//div[@id='control_D0E8F3FA-BD27-4584-B51E-6B49F16D0480']//b)[2]
    click element    xpath:(//div[@id='control_D0E8F3FA-BD27-4584-B51E-6B49F16D0480']//b)[2]

    #Processess to be assessed checklist
    sleep    2 seconds
    #wait until element is visible    xpath:(//a[text()='Environmental']/i[1])[1]
    click element    xpath:(//a[text()='Environmental']/i[1])[1]
    capture page screenshot

    #Type of risk assessment dropdown
    click element    id:control_7980E64B-9FFC-4251-8A14-227AEEFE7309
    #wait until element is visible    xpath://a[text()='Regulatory Risk Assessment']
    click element    xpath://a[text()='Regulatory Risk Assessment']

    #Responsible person dropdown
    click element    id:control_25EB2D38-F2AA-4689-94E5-36BABF04BBF9
    #wait until element is visible    xpath://a[text()='2 Manager']
    click element    xpath://a[text()='2 Manager']

IsthisABowtieYes
    click element    id:control_8CC137FF-3CE3-4C38-8C0B-0D0B0D51993F
    click element    xpath://a[text()='Yes']

IsthisABowtieNo
    click element    id:control_8CC137FF-3CE3-4C38-8C0B-0D0B0D51993F
    click element    xpath://a[text()='No']

SelectApplicableRiskSourceFromBusinessProcessLibrary
    click element    id:control_D4AC91B6-E1EF-447E-AE02-CFBCF78907A1
    click element    xpath://a[text()='Select Applicable Risk Source From Business Process Library']
    #Risk sources checklist
    element should be visible    id:control_A47BCB17-6674-447D-8D0F-84464D500615
    click element    xpath://a[text()='Access control hazards']/../i
    click element    xpath:(//a[text()='Unauthorised access to hazardous areas']/i)[1]
    capture page screenshot

NewBlankRiskAssessment
    click element    id:control_D4AC91B6-E1EF-447E-AE02-CFBCF78907A1
    click element    xpath://a[text()='New Blank Risk Assessment']
    capture page screenshot

CopyFromPreviousRiskAssessment
    click element    id:control_D4AC91B6-E1EF-447E-AE02-CFBCF78907A1
    click element    xpath://a[text()='Copy From Previous Risk Assessment']
    element should be visible    xpath://div[@id='control_5969E967-0B97-46D1-B1CE-12866D712A49']//li
    click element    xpath://div[@id='control_5969E967-0B97-46D1-B1CE-12866D712A49']//li
    click element    xpath://a[text()='2 Auto Risk FR1 AS5 Ant']
    capture page screenshot

SaveNewBlankRiskAssessment
    #Save button
    click element    id:btnSave_form_3AB45051-222E-416C-AAD3-86B2EDA98BED
    set selenium implicit wait    90 seconds
    #Save validation
    ${recordnumber}=    get text    xpath:(//div[@id='form_3AB45051-222E-416C-AAD3-86B2EDA98BED']//div[contains(text(),'- Record #')])[1]
    log    ${recordnumber}
    capture page screenshot
    #Processflow validation
    element text should be    //div[@class='step active']//div[text()='2. Edit / Under Review']    2. Edit / Under Review
    #Status validation
    element text should be    //div[@id='control_87EEE5E5-7E0D-4E0B-A183-DD2E5613D0ED']//li[text()='Open']    Open
    #Requested by validation
    element text should be    //div[@id='control_CE1AFC63-C1A3-4BA7-9BE9-78AB0CC45478']//li[text()='IsoMetrix Administrator']    IsoMetrix Administrator

Save2
    #Save button
    click element    id:btnSave_form_3AB45051-222E-416C-AAD3-86B2EDA98BED
    set selenium implicit wait    240 seconds
    #Save validation
    ${recordnumber}=    get text    xpath:(//div[@id='form_3AB45051-222E-416C-AAD3-86B2EDA98BED']//div[contains(text(),'- Record #')])[1]
    log    ${recordnumber}
    capture page screenshot

    #Processflow validation
    element text should be    //div[@class='step active']//div[text()='2. Edit / Under Review']    2. Edit / Under Review

    #Status validation
    element text should be    //div[@id='control_87EEE5E5-7E0D-4E0B-A183-DD2E5613D0ED']//li[text()='Open']    Open

    #Requested by validation
    element text should be    //div[@id='control_CE1AFC63-C1A3-4BA7-9BE9-78AB0CC45478']//li[text()='IsoMetrix Administrator']    IsoMetrix Administrator

    #Risk Assessment
    click element    xpath://div[@id='control_3BDB074E-C37E-48F3-85C5-CDBE25069F9E']//div[contains(text(),'Drag a column header and drop it here to group by that column')]/../div[3]//tr[1]
    set selenium implicit wait    15 sec
    #wait until element is visible    id:btnProcessFlow_form_3A80F1F4-AE90-47B2-B9BB-0C920589C04D
    click element    id:btnProcessFlow_form_3A80F1F4-AE90-47B2-B9BB-0C920589C04D
    wait until element is visible    xpath:(//div[text()='2. In Progress'])[2]/parent::div
    element text should be    (//div[text()='2. In Progress'])[2]/parent::div    2. In Progress
    capture page screenshot

