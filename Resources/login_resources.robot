*** Settings ***
Library    SeleniumLibrary


*** Variables ***
${browser}  chrome
${url}    https://zaiso-qa02.isometrix.net/isometrix.solutions.v4/default.aspx

*** Keywords ***
Start TestCase
    open browser    ${url}  ${browser}
    maximize browser window

Input UserName
    [Arguments]    ${username}
     input text    id:txtUsername    ${username}

Input Password
    [Arguments]    ${password}
     input text    id:txtPassword   ${password}

Click Signin Button
     click element    xpath://div[@id='btnLoginSubmit']
     capture page screenshot

Capture Title
     title should be    IsoMetrix
     capture page screenshot    IsometrixSuccessfullLogin.png

Error message1
    page should contain    Validation: Please enter value

Error message2
    page should contain    ERROR: The login credentials you entered are incorrect. Please try again

Reset password
    click element    xpath://div[@id='lblForgot']
    set selenium implicit wait    5 sec
    capture page screenshot
    click element    id:lblForgotBack
    capture page screenshot

Request access
    click element    xpath://div[@id='lblAccess']
    set selenium implicit wait    3 sec
    capture page screenshot
    click element    id:lblAccessBack
    capture page screenshot


Finish TestCase
    close browser
