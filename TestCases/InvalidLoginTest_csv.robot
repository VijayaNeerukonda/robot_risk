*** Settings ***
Library  SeleniumLibrary
Resource    ../Resources/login_resources.robot
Library    DataDriver   ../TestData/LoginData.csv
Suite Setup    Start TestCase
#Suite Teardown    Finish TestCase
Test Template    Invalid Login

*** Test Cases ***
LoginTestwithExcel using ${username} and ${password}

*** Keywords ***
Invalid Login
    [Arguments]    ${username}    ${password}    ${errormessage}    ${resetpassword}    ${requestaccess}
    set selenium implicit wait    10 seconds
    run keyword if    "${resetpassword}"=="Yes"    Reset password
    ...     ELSE    log    Not resetting password
    run keyword if    "${requestaccess}"=="Yes"    Request access
    ...     ELSE    log    Not requesting access
    Input UserName    ${username}
    Input Password    ${password}
    Click Signin Button
    run keyword if    "${errormessage}"=="Validation: Please enter value"    Error message1
    ...    ELSE IF    "${errormessage}"=="ERROR: The login credentials you entered are incorrect. Please try again "    Error message2
    ...     ELSE    Capture Title
