*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${browser}  chrome
${url}    https://www.office.com
${outlookusername}     Vijaya.Neerukonda@isometrix.com
${outlookpassword}
${username}    2Manager
${password}    Manager@2020

*** Test Cases ***
LoginToOffcie.com
    open browser    ${url}  ${browser}
    maximize browser window
    set selenium implicit wait    10 seconds

    #Signin button
    click element    xpath://a[contains(text(),'Sign in')]
    input text    xpath://input[@type='email']    ${outlookusername}
    click element    xpath://input[@value='Next']
    input text    xpath://input[@type='password']    ${outlookpassword}
    click element    //input[@value='Sign in']
    capture page screenshot

    #Outlook folder
    click element    xpath://a[@id='ShellMail_link']
    capture page screenshot

    #Switch to Tab
    switch window    Email - Vijaya Neerukonda - Outlook

    #Non-Production folder
    wait until element is visible    xpath://span[text()='Non Production']
    click element    xpath://span[text()='Non Production']
    capture page screenshot

    #Risk Register Notification
    click element    xpath:(//span[contains(text(),'Non production - IsoMetrix Risk Register')])[1]
    capture page screenshot

    #Link back to record
    click element    xpath://a[@data-auth='NotApplicable']
    capture page screenshot

    #Switch window
    switch window    IsoMetrix

    #Login to Isometrix site
    input text    id:txtUsername    ${username}
    input text    id:txtPassword    ${password}
    click element    id:btnLoginSubmit
    capture page screenshot

    #Wait for risk record
    set selenium timeout    25 seconds
    wait until page contains    id:btnProcessFlow_form_3AB45051-222E-416C-AAD3-86B2EDA98BED
    click element    id:btnProcessFlow_form_3AB45051-222E-416C-AAD3-86B2EDA98BED
    capture page screenshot





