*** Settings ***
Library  SeleniumLibrary
Resource    ../Resources/login_resources.robot
Suite Setup    Start TestCase
#Suite Teardown    Finish TestCase
Test Template    Invalid Login


*** Test Cases ***           username     password
Empty user empty password    ${EMPTY}       ${EMPTY}
Invalid user empty password       2Man        ${EMPTY}
Empty user right password       ${EMPTY}    Manager@2020
Valid user invalid password       2Manager    ${EMPTY}
Valid user and password     2Manager        Manager@2020


*** Keywords ***
Invalid Login
    [Arguments]    ${username}    ${password}    ${errormessage}    ${resetpassword}    ${requestaccess}
    run keyword if    "${resetpassword}"=="Yes"    Reset password
    ...     ELSE    log    Not resetting password
    run keyword if    "${requestaccess}"=="Yes"    Request access
    ...     ELSE    log    Not requesting access
    Input UserName    ${username}
    Input Password    ${password}
    Click Signin Button
    run keyword if    "${errormessage}"=="Validation: Please enter value"    Error message1
    ...    ELSE IF    "${errormessage}"=="ERROR: The login credentials you entered are incorrect. Please try again "    Error message2
    ...     ELSE    Capture Title

