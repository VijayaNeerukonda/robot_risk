*** Settings ***
Library  SeleniumLibrary
Resource    ../Resources/RiskRegister_Resources.robot
Library    DataDriver   ../TestData/FR1_AS5.csv
Test Template    AddRisk

*** Test Cases ***
FR1_AS5

*** Keywords ***
AddRisk
    [Arguments]    ${username}    ${password}    ${RiskTitle}    ${RiskScope}    ${ReasonForRequest}    ${Other}    ${CreateRiskAssessmentFrom}    ${Project}    ${BowtieRiskAssessment}    ${SaveRisk}
    #set selenium implicit wait    10 seconds
    LoginTest    ${username}    ${password}
    NaviagteToRiskRegister
    ClickOnAddButton
    Log    ${ReasonForRequest}
    click element   xpath://div[@id='control_460A1A95-6C2F-4764-9631-1DDB8912196B']//li
    run keyword if    "${ReasonForRequest}"=="As result of an incident"    AsresultofIncident
    ...    ELSE IF    "${ReasonForRequest}"=="Other (Please specify)"    Other    ${Other}
    ...     ELSE    AnnualReview
    BusinessUnit
    Log    ${Project}
    run keyword if    "${Project}"=="True"    ProjectCheckbox
    ...     ELSE    Log     Project checkbox not selected
    CaptureRemainingDetails    ${RiskTitle}    ${RiskScope}
    Log    ${BowtieRiskAssessment}
    run keyword if    "${BowtieRiskAssessment}"=="Yes"    IsthisABowtieYes
    ...     ELSE    IsthisABowtieNo
    Log    ${CreateRiskAssessmentFrom}
    run keyword if    "${CreateRiskAssessmentFrom}"=="Copy From Previous Risk Assessment"    CopyFromPreviousRiskAssessment
    ...    ELSE IF    "${CreateRiskAssessmentFrom}"=="Select Applicable Risk Source From Business Process Library"    SelectApplicableRiskSourceFromBusinessProcessLibrary
    ...     ELSE    NewBlankRiskAssessment
    run keyword if    "${SaveRisk}"=="New Blank Assessment"    SaveNewBlankRiskAssessment
    ...     ELSE    Save2

