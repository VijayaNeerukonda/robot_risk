*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${Browser}  chrome
${url}  https://zaiso-qa02.isometrix.net/IsoMetrix.Automation.QA/default.aspx

*** Test Cases ***
LoginTest
    Open Browser    ${url}     ${Browser}
    maximize browser window
    LoginDemoProjectSite
    ModuleA1ModuleControl
    Close Browser

*** Keywords ***
LoginDemoProjectSite
    ${"UserName"}   set variable    id:txtUsername
    element should be visible   ${"UserName"}
    element should be enabled   ${"UserName"}
    input text  ${"UserName"}  Isometrixsystem

    ${"Password"}   set variable    id:txtPassword
    element should be visible   ${"Password"}
    element should be enabled   ${"Password"}
    input text  ${"Password"}  1

    ${"SignIn"}     set variable    id:btnLoginSubmit
    click element   ${"SignIn"}
    title should be     IsoMetrix
    set selenium speed      4 seconds

ModuleA1ModuleControl
    sleep    3 seconds
    Select Frame    xpath://iframe[@id='ifrMain']
    set selenium speed      3seconds
    ${"SourcesID"}     set variable    id:section_c07b32b3-2095-4543-a578-367c27a16d16
    click element   ${"SourcesID"}
    set selenium speed      1 seconds

    ${"ModuleC1ID"}     set variable    id:section_c180ff41-48e1-4c8c-8436-700c2c87c169
    click element   ${"ModuleC1ID"}
    set selenium speed      1 seconds

    ${"AddNewBtnID"}     set variable    id:btnActAddNew
    click element   ${"AddNewBtnID"}
    set selenium speed      5 seconds

    ${"ModuleSourcesID"}   set variable  id:tab_B6FDEB7C-B09B-46D0-8302-FE1AEFC56742
    click element   ${"ModuleSourcesID"}
    set selenium speed      1 seconds

        ########################### Module A1 - Textbox ##########################################
        ########################### Making a selection in B1 All #################################
    ${"B1DropdwnXpath"}   set variable  xpath://*[@id="control_7D23DE46-737B-4525-8EA1-972181AE2CC4"]/div[1]/a/span[2]/b[1]
    click element   ${"B1DropdwnXpath"}
    set selenium speed      0seconds

    ${"ExpandA1DrpdownXpath"}   set variable    xpath:/html/body/div[1]/div[3]/div/div[2]/div[21]/ul[1]/ul/li/i
    click element   ${"ExpandA1DrpdownXpath"}
    set selenium speed      0seconds

    ${"B1_A1B1TextboxText"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[21]/ul[1]/ul/li/ul/li/a
    click element   ${"B1_A1B1TextboxText"}
    set selenium speed      0seconds

    ${"ExpandA1TextboxB1Xpath"}   set variable  xpath://*[@id="46e209ed-b8a6-4c00-b7dd-96a67b98f2c1"]/i
    click element   ${"ExpandA1TextboxB1Xpath"}
    set selenium speed      0seconds

    ${"C1_A1B1TextboxXpath"}   set variable  xpath://*[@id="7288358a-149a-4013-8dbc-2afccdae753b_anchor"]/i[1]
    click element   ${"C1_A1B1TextboxXpath"}
    set selenium speed      1seconds

        ############ Making selection in B2 Specific ############################################
    ${"B2SpecificDrpdwnXpath"}   set variable    xpath://*[@id="control_57271D14-027B-4B6C-8E0F-1FACA16EA872"]/div[1]/a/span[2]/b[1]
   click element   ${"B2SpecificDrpdwnXpath"}
    set selenium speed      0seconds

    ${"ExpandA1SpecificDrpdownXpath"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[22]/ul[1]/ul/li/i
    click element   ${"ExpandA1SpecificDrpdownXpath"}
    set selenium speed      0seconds

    ${"B2_A1B1DrpdownText"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[22]/ul[1]/ul/li/ul/li/a
    click element   ${"B2_A1B1DrpdownText"}
    set selenium speed      0seconds

    ${"ExpandB2_A1TextboxXpath"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[7]/div[9]/div[2]/div[2]/div/div/div[9]/div[2]/div[1]/div/div[11]/div[1]/div/ul[1]/ul/li/i
    click element   ${"ExpandB2_A1TextboxXpath"}
    set selenium speed      0seconds

    ${"B2_A1B2TextboxXpath"}   set variable    xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[7]/div[9]/div[2]/div[2]/div/div/div[9]/div[2]/div[1]/div/div[11]/div[1]/div/ul[1]/ul/li/ul/li/a/i[1]
    click element   ${"B2_A1B2TextboxXpath"}
    set selenium speed      1seconds

        ############################ Making selection in B3 Module ###############################
    ${"B3DrpdwnXpath"}   set variable    xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[7]/div[9]/div[2]/div[2]/div/div/div[9]/div[2]/div[1]/div/div[8]/div[1]/a/span[1]/ul/li
    click element   ${"B3DrpdwnXpath"}
    set selenium speed      0seconds

    ${"ExpandB3_A1TextboxXpath"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[23]/ul[1]/ul/li/i
    click element   ${"ExpandB3_A1TextboxXpath"}
    set selenium speed      0seconds

    ${"B3_A1B3Text"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[23]/ul[1]/ul/li/ul/li/a
    click element   ${"B3_A1B3Text"}
    set selenium speed      0seconds

    ${"ExpandA1_B3TextboxXpath"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[7]/div[9]/div[2]/div[2]/div/div/div[9]/div[2]/div[1]/div/div[12]/div[1]/div/ul[1]/ul/li/i
    click element   ${"ExpandA1_B3TextboxXpath"}
    set selenium speed      0seconds

    ${"A1B3TextBoxXpath"}   set variable    xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[7]/div[9]/div[2]/div[2]/div/div/div[9]/div[2]/div[1]/div/div[12]/div[1]/div/ul[1]/ul/li/ul/li/a/i[1]
    click element   ${"A1B3TextBoxXpath"}
    set selenium speed      1seconds

        ############################ Making selection in B4 Inherit ##############################
    ${"B4textboxXpath"}   set variable    xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[7]/div[9]/div[2]/div[2]/div/div/div[9]/div[2]/div[1]/div/div[9]/div[1]/a/span[1]/ul/li
    click element   ${"B4textboxXpath"}
    set selenium speed      0seconds

    ${"ExpandB4_A1Xpath"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[24]/ul[1]/ul/li/i
    click element   ${"ExpandB4_A1Xpath"}
    set selenium speed      0seconds

    ${"A1_B4Text"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[24]/ul[1]/ul/li/ul/li/a
    click element   ${"A1_B4Text"}
    set selenium speed      0seconds

    ${"B4_A1ExpandXpath"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[7]/div[9]/div[2]/div[2]/div/div/div[9]/div[2]/div[1]/div/div[13]/div[1]/div/ul[1]/ul/li/i
    click element   ${"B4_A1ExpandXpath"}
    set selenium speed      0seconds

    ${"A1B4Xpath"}   set variable    xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[7]/div[9]/div[2]/div[2]/div/div/div[9]/div[2]/div[1]/div/div[13]/div[1]/div/ul[1]/ul/li/ul/li/a/i[1]
    click element   ${"A1B4Xpath"}
    set selenium speed      1seconds

        ########################### Module A1 - Numeric Textbox ##############################
        ########################### Making a selection in B1 All #############################
    ${"ALLNumericA1DrpdwnXpath"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[7]/div[9]/div[2]/div[2]/div/div/div[9]/div[2]/div[1]/div/div[15]/div[1]/a/span[1]/ul/li
    click element   ${"ALLNumericA1DrpdwnXpath"}
    set selenium speed      0seconds

    ${"ExpandA1DrpdownXpath"}   set variable    xpath:/html/body/div[1]/div[3]/div/div[2]/div[25]/ul[1]/ul/li/i
    click element   ${"ExpandA1DrpdownXpath"}
    set selenium speed      0seconds

    ${"A1DrpdwnText"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[25]/ul[1]/ul/li/ul/li/a
    click element   ${"A1DrpdwnText"}
    set selenium speed      0seconds

    ${"A1TextboxExpandXpath"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[7]/div[9]/div[2]/div[2]/div/div/div[9]/div[2]/div[1]/div/div[19]/div[1]/div/ul[1]/ul/li/i
    click element   ${"A1TextboxExpandXpath"}
    set selenium speed      0seconds

    ${"A1TextboxValue"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[7]/div[9]/div[2]/div[2]/div/div/div[9]/div[2]/div[1]/div/div[19]/div[1]/div/ul[1]/ul/li/ul/li/a/i[1]
    click element   ${"A1TextboxValue"}
    set selenium speed      1seconds

        ############ Making selection in B2 Specific ########################################
    ${"SpecificNumericDrpdwnXpath"}   set variable    xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[7]/div[9]/div[2]/div[2]/div/div/div[9]/div[2]/div[1]/div/div[16]/div[1]/a/span[1]/ul/li
    click element   ${"SpecificNumericDrpdwnXpath"}
    set selenium speed      0seconds

    ${"ExpandSpecificNumericDrpdownXpath"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[26]/ul[1]/ul/li/i
    click element   ${"ExpandSpecificNumericDrpdownXpath"}
    set selenium speed      0seconds

    ${"SpecificNumericText"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[26]/ul[1]/ul/li/ul/li/a
    click element   ${"SpecificNumericText"}
    set selenium speed      0seconds

    ${"ExpandSpecficNumericXpath"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[7]/div[9]/div[2]/div[2]/div/div/div[9]/div[2]/div[1]/div/div[20]/div[1]/div/ul[1]/ul/li/i
    click element   ${"ExpandSpecficNumericXpath"}
    set selenium speed      0seconds

    ${"SpecificNumerictextboxValueXpath"}   set variable    xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[7]/div[9]/div[2]/div[2]/div/div/div[9]/div[2]/div[1]/div/div[20]/div[1]/div/ul[1]/ul/li/ul/li/a/i[1]
    click element   ${"SpecificNumerictextboxValueXpath"}
    set selenium speed      1seconds

        ############################ Making selection in B3 Module ##########################
    ${"NumericModuleXpath"}   set variable    xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[7]/div[9]/div[2]/div[2]/div/div/div[9]/div[2]/div[1]/div/div[17]/div[1]/a/span[1]/ul/li
    click element   ${"NumericModuleXpath"}
    set selenium speed      0seconds

    ${"ExpandNumericModule"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[27]/ul[1]/ul/li/i
    click element   ${"ExpandNumericModule"}
    set selenium speed      0seconds

    ${"NumericModuleText"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[27]/ul[1]/ul/li/ul/li/a
    click element   ${"NumericModuleText"}
    set selenium speed      0seconds

    ${"ExpandNumericModuleTextboxXpath"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[7]/div[9]/div[2]/div[2]/div/div/div[9]/div[2]/div[1]/div/div[21]/div[1]/div/ul[1]/ul/li/i
    click element   ${"ExpandNumericModuleTextboxXpath"}
    set selenium speed      0seconds

    ${"NumericModuleTextboxValue"}   set variable    xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[7]/div[9]/div[2]/div[2]/div/div/div[9]/div[2]/div[1]/div/div[21]/div[1]/div/ul[1]/ul/li/ul/li/a/i[1]
    click element   ${"NumericModuleTextboxValue"}
    set selenium speed      1seconds

        ############################ Making selection in B4 Inherit #########################
    ${"NumericEnheritDrpXpath"}   set variable    xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[7]/div[9]/div[2]/div[2]/div/div/div[9]/div[2]/div[1]/div/div[18]/div[1]/a/span[1]/ul/li
    click element   ${"NumericEnheritDrpXpath"}
    set selenium speed      0seconds

    ${"ExpandNumericEnherit"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[28]/ul[1]/ul/li/i
    click element   ${"ExpandNumericEnherit"}
    set selenium speed      0seconds

    ${"NumericEnhritText"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[28]/ul[1]/ul/li/ul/li/a
    click element   ${"NumericEnhritText"}
    set selenium speed      0seconds

    ${"ExpandNumericEnheritTextboxXpath"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[7]/div[9]/div[2]/div[2]/div/div/div[9]/div[2]/div[1]/div/div[22]/div[1]/div/ul[1]/ul/li/i
    click element   ${"ExpandNumericEnheritTextboxXpath"}
    set selenium speed      0seconds

    ${"NumericEnheritTextboxValue"}   set variable    xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[7]/div[9]/div[2]/div[2]/div/div/div[9]/div[2]/div[1]/div/div[22]/div[1]/div/ul[1]/ul/li/ul/li/a/i[1]
    click element   ${"NumericEnheritTextboxValue"}
    set selenium speed      1seconds

        ########################### Module A1 - Text Area ##################################
        ########################### Making a selection in B1 All ###########################
    ${"ALLNumericA1DrpdwnXpath"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[7]/div[9]/div[2]/div[2]/div/div/div[9]/div[2]/div[1]/div/div[24]/div[1]/a/span[1]/ul/li
    click element   ${"ALLNumericA1DrpdwnXpath"}
    set selenium speed      0seconds

    ${"ExpandA1DrpdownXpath"}   set variable    xpath:/html/body/div[1]/div[3]/div/div[2]/div[29]/ul[1]/ul/li/i
    click element   ${"ExpandA1DrpdownXpath"}
    set selenium speed      0seconds

    ${"A1DrpdwnText"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[29]/ul[1]/ul/li/ul/li/a
    click element   ${"A1DrpdwnText"}
    set selenium speed      0seconds

    ${"A1TextboxExpandXpath"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[7]/div[9]/div[2]/div[2]/div/div/div[9]/div[2]/div[1]/div/div[28]/div[1]/div/ul[1]/ul/li/i
    click element   ${"A1TextboxExpandXpath"}
    set selenium speed      0seconds

    ${"A1TextboxValue"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[7]/div[9]/div[2]/div[2]/div/div/div[9]/div[2]/div[1]/div/div[28]/div[1]/div/ul[1]/ul/li/ul/li/a/i[1]
    click element   ${"A1TextboxValue"}
    set selenium speed      1seconds

        ######################### Making selection in B2 Specific ##########################
    ${"SpecificNumericDrpdwnXpath"}   set variable    xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[7]/div[9]/div[2]/div[2]/div/div/div[9]/div[2]/div[1]/div/div[25]/div[1]/a/span[1]/ul/li
    click element   ${"SpecificNumericDrpdwnXpath"}
    set selenium speed      0seconds

    ${"ExpandSpecificNumericDrpdownXpath"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[30]/ul[1]/ul/li/i
    click element   ${"ExpandSpecificNumericDrpdownXpath"}
    set selenium speed      0seconds

    ${"SpecificNumericText"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[30]/ul[1]/ul/li/ul/li/a
    click element   ${"SpecificNumericText"}
    set selenium speed      0seconds

    ${"ExpandSpecficNumericXpath"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[7]/div[9]/div[2]/div[2]/div/div/div[9]/div[2]/div[1]/div/div[29]/div[1]/div/ul[1]/ul/li/i
    click element   ${"ExpandSpecficNumericXpath"}
    set selenium speed      0seconds

    ${"SpecificNumerictextboxValueXpath"}   set variable    xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[7]/div[9]/div[2]/div[2]/div/div/div[9]/div[2]/div[1]/div/div[29]/div[1]/div/ul[1]/ul/li/ul/li/a/i[1]
    click element   ${"SpecificNumerictextboxValueXpath"}
    set selenium speed      1seconds

        ############################ Making selection in B3 Module #########################
    ${"NumericModuleXpath"}   set variable    xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[7]/div[9]/div[2]/div[2]/div/div/div[9]/div[2]/div[1]/div/div[26]/div[1]/a/span[1]/ul/li
    click element   ${"NumericModuleXpath"}
    set selenium speed      0seconds

    ${"ExpandNumericModule"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[31]/ul[1]/ul/li/i
    click element   ${"ExpandNumericModule"}
    set selenium speed      0seconds

    ${"NumericModuleText"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[31]/ul[1]/ul/li/ul/li/a
    click element   ${"NumericModuleText"}
    set selenium speed      0seconds

    ${"ExpandNumericModuleTextboxXpath"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[7]/div[9]/div[2]/div[2]/div/div/div[9]/div[2]/div[1]/div/div[30]/div[1]/div/ul[1]/ul/li/i
    click element   ${"ExpandNumericModuleTextboxXpath"}
    set selenium speed      0seconds

    ${"NumericModuleTextboxValue"}   set variable    xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[7]/div[9]/div[2]/div[2]/div/div/div[9]/div[2]/div[1]/div/div[30]/div[1]/div/ul[1]/ul/li/ul/li/a/i[1]
    click element   ${"NumericModuleTextboxValue"}
    set selenium speed      1seconds

        ############################ Making selection in B4 Inherit ########################
    ${"NumericEnheritDrpXpath"}   set variable    xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[7]/div[9]/div[2]/div[2]/div/div/div[9]/div[2]/div[1]/div/div[27]/div[1]/a/span[1]/ul/li
    click element   ${"NumericEnheritDrpXpath"}
    set selenium speed      0seconds

    ${"ExpandNumericEnherit"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[32]/ul[1]/ul/li/i
    click element   ${"ExpandNumericEnherit"}
    set selenium speed      0seconds

    ${"NumericEnhritText"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[32]/ul[1]/ul/li/ul/li/a
    click element   ${"NumericEnhritText"}
    set selenium speed      0seconds

    ${"ExpandNumericEnheritTextboxXpath"}   set variable  xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[7]/div[9]/div[2]/div[2]/div/div/div[9]/div[2]/div[1]/div/div[31]/div[1]/div/ul[1]/ul/li/i
    click element   ${"ExpandNumericEnheritTextboxXpath"}
    set selenium speed      0seconds

    ${"NumericEnheritTextboxValue"}   set variable    xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[4]/div[7]/div[9]/div[2]/div[2]/div/div/div[9]/div[2]/div[1]/div/div[31]/div[1]/div/ul[1]/ul/li/ul/li/a/i[1]
    click element   ${"NumericEnheritTextboxValue"}
    set selenium speed      1seconds

    ${"SaveBtnXpath"}   set variable    xpath:/html/body/div[1]/div[3]/div/div[2]/div[2]/div[2]/div[2]/div[1]/div[3]
    click element    ${"SaveBtnXpath"}
    set selenium speed      3seconds
